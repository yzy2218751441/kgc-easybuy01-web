//获得分类列表
function getClassify(){
    jQuery.ajax({
        url:"/kgc-easybuy01/getClassify",
        success:function (firstCategories) {
            for (i=0;i<firstCategories.length;i++){
                var secondCategories=firstCategories[i].childCategorys;
                var $li=jQuery("<li></li>");
                var div1='<div class="fj"><span class="n_img"><span></span><img src="images/nav1.png" /></span><span class="fl">'+firstCategories[i].name+'</span></div>';
                var $div11=jQuery(div1);
                var $div12=jQuery('<div class="zj"></div>');
                var $div13=jQuery('<div class="zj_l"></div>');
                var $div14=jQuery('<div class="zj_r"><a href="#"><img src="images/n_img1.jpg" width="236" height="200" /></a><a href="#"><img src="images/n_img2.jpg" width="236" height="200" /></a></div>');
                for (j=0;j<secondCategories.length;j++){
                    var thirdCategories=secondCategories[j].childCategorys;
                    var $div21=jQuery('<div class="zj_l_c"></div>');
                    var h2='<h2><a href="productCategory.html?categoryLevel=categoryLevel2Id&categoryId='+secondCategories[j].id+'">'+secondCategories[j].name+'</a></h2>';
                    var $h2=jQuery(h2);
                    $div21.append($h2);
                    for(k=0;k<thirdCategories.length;k++){
                        var $a=jQuery('<a href="productCategory.html?categoryLevel=categoryLevel3Id&categoryId='+thirdCategories[k].id+'">'+thirdCategories[k].name+'</a>');
                        $div21.append($a);
                    }
                    $div13.append($div21);
                }
                $div12.append($div13);
                $div12.append($div14);
                $li.append($div12);
                $li.append($div11);
                jQuery("#categoryul").append($li);
            }
            //分类列表的显示
            jQuery(".leftNav ul li").hover(
                function(){
                    jQuery(this).find(".fj").addClass("nuw");
                    jQuery(this).find(".zj").show();
                }
                ,
                function(){
                    jQuery(this).find(".fj").removeClass("nuw");
                    jQuery(this).find(".zj").hide();
                }
            )
        },
        error:function () {
            alert("回调失败！")
        }
    });
}