 jQuery.noConflict();
jQuery(function () {
    getUser();
    console.log(jQuery(".i_car"));
});
//拿到用户登陆状态
function getUser(){
    jQuery.ajax({
        url:"/kgc-easybuy01/getUser",
        success:function (user) {
            if(user.loginName==null){
                //未登陆<span class="fl" id="head11">   </span>
                jQuery("#head11").html("你好，请<a href='login.html'>登录</a>&nbsp; <a href='regist.html' style='color:#ff4e00;'>免费注册</a>&nbsp;");
            }else if (user.type==1){
                //管理员
                jQuery("#head11").html("你好，"+user.loginName+"&nbsp;|&nbsp; <a href='Center.html'>个人中心</a>&nbsp;|&nbsp; <a href='admin.html'>后台管理</a>&nbsp;|&nbsp; <a href='javascript:;' onclick='logout()'>注销</a>");
            }else{
                //普通用户
                jQuery("#head11").html("你好，"+user.loginName+"&nbsp;|&nbsp; <a href='Center.html'>个人中心</a>&nbsp;|&nbsp; <a href='javascript:;' onclick='logout()'>注销</a>");
            }
        },
        error:function () {
            alert("回调失败！")
        }
    });
}

//注销
function logout() {
    jQuery.ajax({
        url:"/kgc-easybuy01/logout",
        success:function (map) {
            if(map.result=="true"){
                delCookie('userId');
                //跳转到登陆页面
                var href="/login.html";
                window.location.href =href ;
            }
        },
        error:function () {
            alert("回调失败！")
        }
    });
}
 //删除cookie
 function delCookie(name){
     var exp = new Date();
     exp.setTime(exp.getTime() - 1);
     var cval=getCookie(name);
     if(cval!=null){
         document.cookie= name + "="+cval+";expires="+exp.toGMTString();
     }
 }
 function getCookie(name){
     var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
     if(arr=document.cookie.match(reg)){
         return unescape(arr[2]);
     }else{
         return null;
     }
 }