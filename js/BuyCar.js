jQuery.noConflict();
jQuery(function () {
    getCart1();
});

//从后台获取购物车和用户
function getCart1() {
    jQuery.post("/kgc-easybuy01/getCart",function (data) {
        //获取页面表格
        var tb=jQuery(".car_tab").find("tbody");
        console.log(tb);
        //内容
        var str="";
        var map=data.cart.map;
        console.log(map);
        //总计
        var total=0.00;
        tb.html("");
        for(var key in map){
            //商品项
            var shopItem=map[key];
            //商品
            var product=shopItem.product;
            total+=shopItem.subtotal;
            //拼接商品项tr
            //先清空tb
            str="<tr><td align='center'><div class='c_s_img'><img src='" +
                "images/"+product.fileName +
                "' width='73' height='73' /><input type='hidden'value='" +
                product.id+
                "'/><input type='hidden' value='" +
                product.stock+
                "'/></div>" +
                product.name +
                "</td><td align='center'>" +
                "￥"+product.price+
                "</td><td align='center'><div class='c_num'><input type='button' value='' onclick='jianUpdate1(jq(this));' class='car_btn_1' /> <input type='text' value='" +
                shopItem.count+
                "' name='' class='car_ipt' readonly='readonly'/><input type='button' value='' onclick='addUpdate1(jq(this));' class='car_btn_2' /></div></td><td align='center' style='color:#ff4e00;'>" +
                "￥"+shopItem.subtotal+
                "</td><td align='center'><a onclick=ShowDiv2('MyDiv','fade',jq(this))>" +
                "删除" +
                "</a>&nbsp; &nbsp;<a onclick=ShowDiv('MyDiv1','fade1',jq(this))>" +
                "收藏" +
                "</a></td></tr>";
            //向表格添加数据
            tb.append(str);
            jQuery(".car_btn_1").live("click",function () {
                getCart1();
            })
            jQuery(".car_btn_2").live("click",function () {
                getCart1();
            })
        }
        //总金额内容
        str="<tr height='70'><td colspan='5' style='font-family:'Microsoft YaHei'; border-bottom:0;'><span class='fr'>" +
            "商品总价" +
            "<b style='font-size:22px; color:#ff4e00;'>" +
            "￥"+total+
            "</b></span></td></tr>";
        tb.append(str);
        //购物车图标部分
        var i=0;
        jQuery(".cars").html("");
        for(var key in map){
            i++;
            console.log(map[key]);
            console.log(map[key].product);
            str="<li><div class='img'><a href='#'><img src='" +
                "images/"+map[key].product.fileName+
                "' width='58' height='58' /></a></div><div class='name'><a href='#'>" +
                map[key].product.name+
                "</a></div><div class='price'><font color='#ff4e00'>" +
                "￥"+map[key].subtotal+
                "</font>"+
                " X"+map[key].count+
                "</div></li>";
            jQuery(".cars").append(str);
        }
        jQuery(".car_t").find("span")[0].innerHTML=i;
        str="<div class='price_sum'>共计&nbsp; <font color='#ff4e00'>￥</font><span>" +
            data.cart.total+
            "</span></div><div class='price_a'><a href='/BuyCar.html'>去购物车结算</a></div>";
        jQuery(".dfoot").html(str);
    })
}
