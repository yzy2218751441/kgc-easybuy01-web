//拿到该分类下的商品表




jQuery.noConflict();
function getProductByCategory() {
    var categoryLevel=jQuery.getUrlParam("categoryLevel");
    var categoryId=jQuery.getUrlParam("categoryId");
    var pageNow=jQuery("#pageNow").val();
    var priceSort=jQuery("#priceSort").val();
    var dataJ={"categoryLevel":categoryLevel,"categoryId":categoryId,"priceSort":priceSort,"pageNow":pageNow};
    selectProduct(dataJ)
}

function selectProduct(dataJ) {
    jQuery.ajax({
        url:"/kgc-easybuy01/getProductByCategory",
        dataType:"json",
        data:dataJ,
        success:function (data) {
            var m='MyDiv';
            var f='fade';
            //清空商品列表
            jQuery(".cate_list").empty();
            var products=data.list;
            for (i=0;i<products.length;i++){
                var $li=jQuery('<li></li>');
                var $div11=jQuery('<div onclick="goProduct(' +
                    products[i].id+
                    ')" class="img"><a href="#"><img src="images/'+products[i].fileName+'" width="210" height="185" /></a></div>')
                var $div12=jQuery('<div class="price"><font>￥<span>'+products[i].price+'</span></font> &nbsp; 26R</div>')
                var $div13=jQuery('<div class="name"><a href="#">'+products[i].name+'</a></div>')
                //收藏 加购物车
                var $div14="<div class='carbg'><a onclick=ShowDiv('MyDiv','fade'," +
                    products[i].id +
                    ") class='ss'>收藏</a><a onclick=goProduct(" +
                    products[i].id+
                    ") class='j_car'>加入购物车</a></div>"
                $li.append($div11);
                $li.append($div12);
                $li.append($div13);
                $li.append($div14);
                jQuery(".cate_list").append($li);
            }
            //删除分页按钮
            jQuery(".cate_listp").remove();
            //拼分页按钮
            var pageNow=parseInt(data.pageNow);
            var totolePage=parseInt(data.totolePage);
            var $p;
            if (pageNow==1){
                $p=jQuery('<p class="cate_listp">共'+data.rowCount+'条&nbsp;&nbsp;&nbsp;'+data.pageNow+'/'+data.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);"onclick="method1(3)">下一页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(4)">尾页</a> '+
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>');
            }else if(1<pageNow &&pageNow<totolePage){
                $p=jQuery('<p class="cate_listp">共'+data.rowCount+'条&nbsp;&nbsp;&nbsp;'+data.pageNow+'/'+data.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);" onclick="method1(1)">首页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(2)">上一页</a>&nbsp;'+
                    '<a href="javascript:void(0);"onclick="method1(3)">下一页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(4)">尾页</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '+
                    '跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>');

            }else if(pageNow==totolePage){
                $p=jQuery('<p class="cate_listp">共'+data.rowCount+'条&nbsp;&nbsp;&nbsp;'+data.pageNow+'/'+data.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);" onclick="method1(1)">首页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(2)">上一页</a>&nbsp;'+
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>')
            }
            jQuery(".cate_list").after($p);
            //将总页数放入页面隐藏域中
            jQuery("#totolePage").val(data.totolePage);

        },
        error:function(data){
            alert("对不起，没有进入回调");
        }
    })
}

//分页跳转函数
function method1(type) {

    (function (jQuery) {
        jQuery.getUrlParam = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
    })(jQuery);

    //获取所需的参数
    var categoryLevel=jQuery.getUrlParam("categoryLevel");
    var categoryId=jQuery.getUrlParam("categoryId");
    var pageNow=parseInt(jQuery("#pageNow").val());
    if (type==1){
        pageNow=1;
        jQuery("#pageNow").val(pageNow);
    }else if(type==2){
        pageNow=pageNow-1;
        jQuery("#pageNow").val(pageNow);
    }else if (type==3){
        pageNow=pageNow+1;
        jQuery("#pageNow").val(pageNow);
    }else if (type==4){
        pageNow=parseInt(jQuery("#totolePage").val());
        jQuery("#pageNow").val(pageNow);
        alert(pageNow);
    }else if(type==5){
        var pageNo=parseInt(jQuery("#pageNo").val());
        var re =  /^[1-9]\d*$/;
        if(!re.test(pageNo)||pageNo>totolePage){
            //如果输入的不是正整数或者大于总页数，默认跳到第一页
            pageNow=1;
        }else{
           pageNow=pageNo;
        }
        jQuery("#pageNow").val(pageNow);
    }
    var priceSort=jQuery("#priceSort").val();
    var dataJ2={"categoryLevel":categoryLevel,"categoryId":categoryId,"pageNow":pageNow,"priceSort":priceSort,"pageNow":pageNow};
    selectProduct(dataJ2);

}

//按价格升序降序排列
function method2() {
    (function (jQuery) {
        jQuery.getUrlParam = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
    })(jQuery);

    //获取所需的参数
    var categoryLevel=jQuery.getUrlParam("categoryLevel");
    var categoryId=jQuery.getUrlParam("categoryId");
    var pageNow=parseInt(jQuery("#pageNow").val());
    //改变排序方式
    var priceSort=parseInt(jQuery("#priceSort").val());
    priceSort=priceSort*-1;
    jQuery("#priceSort").val(priceSort);
    var dataJ2={"categoryLevel":categoryLevel,"categoryId":categoryId,"pageNow":pageNow,"priceSort":priceSort,"pageNow":pageNow};
    selectProduct(dataJ2);
}
//商品详情跳转
function goProduct(id) {
    window.location.href="/Product.html?id="+id;
}

/*收藏部分*/
function ShowDiv(show_div,bg_div,id){
    //获取登录状态码
    var status=jQuery(".car_t").find("input")[0].value;
    if(status=="0"){
        window.location.href ="/login.html";
    }else {
        //将商品加入收藏
        addColl(id,show_div);
        document.getElementById(show_div).style.display='block';
        document.getElementById(bg_div).style.display='block';
        var bgdiv = document.getElementById(bg_div);
        bgdiv.style.width = document.body.scrollWidth;
        // bgdiv.style.height = $(document).height();
        jQuery("#"+bg_div).height(jQuery(document).height());
    }
};

function CloseDiv(show_div,bg_div)
{
    document.getElementById(show_div).style.display='none';
    document.getElementById(bg_div).style.display='none';
};
//收藏操作
function addColl(id,show_div) {
    jQuery.post("/kgc-easybuy01/addColl",{id:id},function (data) {
        //收藏弹出内容
        var span=jQuery("#"+show_div)[0].children[0].children[1].children[0].children[0].children[0].children[1].children[0];
        console.log(span);
        var s=data.s;
        if(s=='ADDSUCCESS'){
            span.innerHTML='您已成功收藏该商品';
        }else if(s=='ADDFAIL'){
            span.innerHTML='收藏数量已达上限';
        }else if(s=='CANCEL'){
            span.innerHTML='您已取消收藏成功';
        }
    })
}