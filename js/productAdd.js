
//拿到一级分类并放入一级下拉列表中
function getCategoryLevel1Id() {
    jQuery.ajax({
        url:"/kgc-easybuy01/getCategoryLevel1Id",
        success:function (categorys) {
            for(i=0;i<categorys.length;i++){
                jQuery(".categoryLevel1Id").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
            }
        },
        error:function () {
            alert("回调失败！")
        }
    })
}

//一级分类选择后拿到二级分类
jQuery(".categoryLevel1Id").click(function() {
    jQuery(".categoryLevel2Id").empty();
    jQuery(".categoryLevel3Id").empty();
    var parentId=jQuery(".categoryLevel1Id").val();
    var dataJ={"parentId":parentId};
    jQuery.ajax({
        url:"/kgc-easybuy01/getCategoryLevel2Id",
        dataType:"json",
        data:dataJ,
        success:function (categorys) {
            for(i=0;i<categorys.length;i++){
                jQuery(".categoryLevel2Id").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
            }
            jQuery(".categoryLevel2Id").append("<option value=''>"+"空"+"</option>");
        },
        error:function () {
            alert("回调失败！")
        }
    })
});

//二级分类选择后拿到三级分类
jQuery(".categoryLevel2Id").click(function() {
    jQuery(".categoryLevel3Id").empty();
    var parentId=jQuery(".categoryLevel2Id").val();
    var dataJ={"parentId":parentId};
    jQuery.ajax({
        url:"/kgc-easybuy01/getCategoryLevel2Id",
        dataType:"json",
        data:dataJ,
        success:function (categorys) {
            for(i=0;i<categorys.length;i++){
                jQuery(".categoryLevel3Id").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
            }
            jQuery(".categoryLevel3Id").append("<option value=''>"+"空"+"</option>");
        },
        error:function () {
            alert("没有子分类啦！")
        }
    })
});

//商品名称非空
jQuery(".name").blur(function(){
    var name=jQuery(this).val();
    jQuery("#name").text("");
    if(name==""){
        jQuery("#name").text("商品名不能为空");
    }else{
        jQuery("#name").text("");
    }
})

//单价非空判断
jQuery(".price").blur(function(){
    var price=jQuery(this).val();
    jQuery("#price").text("");
    if(price==""){
        jQuery("#price").text("商品单价不能为空");
    }else{
        var re =  /^[+]{0,1}(\d+)$|^[+]{0,1}(\d+\.\d+)$/;
        if(!re.test(price)){
            //如果输入的不是正浮点数
            jQuery("#price").text("请输入正数！");
        }else{
            jQuery("#price").text("");
        }
    }
})

//库存非空判断
jQuery(".stock").blur(function(){
    var stock=jQuery(this).val();
    jQuery("#stock").text("");
    if(stock==""){
        jQuery("#stock").text("商品名不能为空");
    }else{
        var re =  /^[1-9]\d*$/;
        if(!re.test(stock)){
            //如果输入的不是正整数
            jQuery("#stock").text("请输入正整数！");
        }else{
            jQuery("#stock").text("");
        }
    }
})

//图片上传前端验证
function upLoad(){
    jQuery("#img").attr('src',"");
    jQuery("#fileName1").text("");
    //使用类型判断浏览器是否支持base64转换
    if (typeof (FileReader) === 'undefined') {
        alert("抱歉，你的浏览器不支持文件读取，请更新浏览器操作！");
    }else{
        var fileInput = document.getElementById("fileName");
        var file = fileInput.files[0];
        //000.获取文件
        var fileSize = fileInput.files[0].size;	//文件大小
        var size = fileSize / 1024*1024; 	//和上面那句一样1.5M约等于160 0000
        //001.文件格式校验
        if (!/image\/\w+/.test(file.type)) {
            jQuery("#fileName1").text("请确保文件为图像类型！");
        }else if(size>1000000){
            jQuery("#fileName1").text("上传图片大小不得大于1M！");
        }else{
            //创建读取文件的对象
            var reader = new FileReader();
            //创建文件读取相关的变量
            var imgFile;
            //为文件读取成功设置事件
            reader.onload=function(e) {
                // alert('文件读取完成');
                imgFile = e.target.result;
                console.log(imgFile);
                jQuery("#img").attr('src',imgFile);
            };
            jQuery("#img").show();
            //正式读取文件
            reader.readAsDataURL(file);
        }
    }
}

/*
 //添加商品
 function addProduct() {
 if(jQuery(".name").val()!=""&&jQuery("#name").text()==""&&jQuery(".price").val()!=""&&jQuery("#price").text()==""&&
 jQuery(".stock").val()!=""&&jQuery("#stock").text()==""&&jQuery("#fileName").val().length>0&&jQuery("#fileName1").text()==""){
 //添加操作
 jQuery.ajaxFileUpload({
 url:"/kgc-easybuy01/addProduct",
 secureuri:false,
 fileElement:"fileName",
 dataType:"json",
 success: function (result) {
 if (parseInt(result) != 0) {
 alert("添加成功！");
 //window.location.href = "productAdmin.html?pageNow=1";
 }
 },
 error: function (data) {
 alert("回调失败！");
 }
 })
 /!*var formData=new FormData(jQuery(".mem_tab")[0]);
 jQuery.ajax({
 url: "/kgc-easybuy01/addProduct",
 type:"POST",
 enctype: 'multipart/form-data',
 data: formData,
 processData:false,
 contentType:false,
 success: function (result) {
 if (parseInt(result) != 0) {
 alert("添加成功！");
 //window.location.href = "productAdmin.html?pageNow=1";
 }
 },
 error: function (data) {
 alert("回调失败！");
 }
 });*!/
 }
 }

 */
