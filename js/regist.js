//给所有输入框做失焦验证
//给用户名做防止重复验证
jQuery(".l_user").blur(function(){
    var userName=jQuery(this).val();
    jQuery("#l_user").text("");
    if(userName==""){
        jQuery("#l_user").text("用户名不能为空");
    }else{
        jQuery.ajax({
            url:"/kgc-easybuy01/registTest",
            data:"loginName="+userName,
            success:function(user){
                if(user.loginName==null){
                    jQuery("#l_user").text("");
                }else{
                    jQuery("#l_user").text("用户名已存在");
                }
            },
            error:function(data){
                alert("回调失败！");
            }
        });
    }
})

jQuery(".l_pwd1").blur(function(){
    var pwd1=jQuery(this).val();
    jQuery("#l_pwd1").text("");
    if(pwd1==""){
        jQuery("#l_pwd1").text("密码不能为空");
    }
})

jQuery(".IDCard").blur(function(){
    var IDCard=jQuery(this).val();
    var patrn=/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    var result=patrn.test(IDCard);
    jQuery("#IDCard").text("");
    if(IDCard==""){
        jQuery("#IDCard").text("身份证不能为空");
    }else if(result==false){
        jQuery("#IDCard").text("身份证格式不正确");
    }
})

jQuery(".l_pwd2").blur(function(){
    var pwd2=jQuery(this).val();
    var pwd1=jQuery(".l_pwd1").val();
    jQuery("#l_pwd2").text("");
    if(pwd2==""){
        jQuery("#l_pwd2").text("密码不能为空");
    }else if(pwd1!=pwd2){
        jQuery("#l_pwd2").text("两次输入密码不一致");
        jQuery("#l_pwd1").text("两次输入密码不一致");
    }else if(pwd1==pwd2){
        jQuery("#l_pwd2").text("");
        jQuery("#l_pwd1").text("");
    }
})
jQuery(".l_email").blur(function(){
    var l_email=jQuery(this).val();
    var patrn=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
    var result=patrn.test(l_email);
    jQuery("#l_email").text("");
    if(l_email==""){
        jQuery("#l_email").text("邮箱不能为空");
    }else if(result==false){
        jQuery("#l_email").text("邮箱格式不正确");
    }
})
jQuery(".l_tel").blur(function(){
    var l_tel=jQuery(this).val();
    var patrn=/^1(3|4|5|6|7|8|9)\d{9}$/;
    var result=patrn.test(l_tel);
    jQuery("#l_tel").text("");
    if(l_tel==""){
        jQuery("#l_tel").text("电话号码不能为空");
    }else if(result==false){
        jQuery("#l_tel").text("电话号码格式错误");
    }
})

//注册功能
jQuery(".log_btn").click(function(){
    var l_user=jQuery(".l_user").val();
    var l_pwd1=jQuery(".l_pwd1").val();
    var l_pwd2=jQuery(".l_pwd2").val();
    var l_email=jQuery(".l_email").val();
    var l_tel=jQuery(".l_tel").val();
    var IDCard=jQuery(".IDCard").val();
    console.log(l_user+l_pwd1);
    if(l_user!="" && l_pwd1!=""&&l_pwd2!=""&&l_email!=""&&l_tel!=""&&IDCard!=""&&jQuery("#l_user").text()==""&&jQuery("#l_pwd1").text()==""
        &&jQuery("#l_pwd2").text()==""&&jQuery("#l_email").text()==""&&jQuery("#l_tel").text()==""&&jQuery("#IDCard").text()==""){
        var sex = jQuery("input[name='sex']:checked").val();
        var v_json={"loginName":l_user,"password":l_pwd1,"email":l_email,"mobile":l_tel,"sex":sex,"type":"0","identityCode":IDCard};
        jQuery.ajax({
            type:"post",
            url:"/kgc-easybuy01/regist",
            data: v_json,
            dataType : "json",
            success:function(map){
                if(map.result!="0"){
                    alert("注册成功");
                    var href="login.html";
                    window.location.href =href ;
                }else{
                    alert("注册失败")
                }
            },
            error:function(data){
                alert("对不起，没有进入回调");
            }
        });
    }else{
        alert("注册失败");
    }
});