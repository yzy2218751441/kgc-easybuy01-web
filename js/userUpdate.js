//从URL中获取参数
(function (jQuery) {
    jQuery.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
})(jQuery);

//表单回显
function getUser() {
    var id=jQuery.getUrlParam("id");
    var dataJ={"id":id};
    jQuery.ajax({
        url:"/kgc-easybuy01/getTheUser",
        dataType:"json",
        data:dataJ,
        success:function (user) {
            jQuery(".loginName").val(user.loginName);
            jQuery(".userName").val(user.userName);
            jQuery(".identityCode").val(user.identityCode);
            jQuery(".email").val(user.email);
            jQuery(".mobile").val(user.mobile);
            jQuery("#loginName1").val(user.loginName);
            jQuery("#id").val(user.id);
            if(user.type=="0"){
                jQuery(".selector").val("0");
            }else if(user.type=="1"){
                jQuery(".selector").val("1");
            }
        },
        error:function () {
            alert("回调失败！");
        }
    })
}

//对登录名做验证
jQuery(".loginName").blur(function(){
    var loginName=jQuery(this).val();
    var loginName1=jQuery("#loginName1").val();
    jQuery("#loginName").text("");
    if(loginName==""){
        jQuery("#loginName").text("用户名不能为空");
    }else{
        jQuery.ajax({
            url:"/kgc-easybuy01/registTest",
            data:"loginName="+loginName,
            success:function(user){
                if(user.loginName==null||user.loginName==loginName1){
                    jQuery("#loginName").text("");
                }else{
                    jQuery("#loginName").text("用户名不可用");
                }
            },
            error:function(data){
                alert("回调失败！");
            }
        });
    }
})

//身份证验证
jQuery(".identityCode").blur(function(){
    var identityCode=jQuery(this).val();
    var patrn=/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    var result=patrn.test(identityCode);
    jQuery("#identityCode").text("");
    if(identityCode==""){
        jQuery("#identityCode").text("身份证不能为空");
    }else if(result==false){
        jQuery("#identityCode").text("身份证格式不正确");
    }
})
//邮箱验证
jQuery(".email").blur(function(){
    var email=jQuery(this).val();
    var patrn=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
    var result=patrn.test(email);
    jQuery("#email").text("");
    if(email==""){
        jQuery("#email").text("邮箱不能为空");
    }else if(result==false){
        jQuery("#email").text("邮箱格式不正确");
    }
})
//手机号验证
jQuery(".mobile").blur(function(){
    var mobile=jQuery(this).val();
    var patrn=/^1(3|4|5|6|7|8|9)\d{9}$/;
    var result=patrn.test(mobile);
    jQuery("#mobile").text("");
    if(mobile==""){
        jQuery("#mobile").text("电话号码不能为空");
    }else if(result==false){
        jQuery("#mobile").text("电话号码格式错误");
    }
})

//修改操作
function update() {
    var loginName=jQuery(".loginName").val();
    var id=jQuery("#id").val();
    var userName=jQuery(".userName").val();
    var email=jQuery(".email").val();
    var mobile=jQuery(".mobile").val();
    var identityCode=jQuery(".identityCode").val();
    var type=jQuery(".selector").val();
    if(loginName!=""&&email!=""&&mobile!=""&&identityCode!=""&&jQuery("#loginName").text()==""&&jQuery("#email").text()==""
        &&jQuery("#mobile").text()==""&&jQuery("#identityCode").text()==""){
        var v_json={"id":id,"loginName":loginName,"userName":userName,"email":email,"mobile":mobile,"type":type,"identityCode":identityCode};
        jQuery.ajax({
            url:"/kgc-easybuy01/update",
            data: v_json,
            dataType : "json",
            success:function(map){
                if(parseInt(map.result)>0){
                    //修改成功，跳转到查询页
                    alert("修改成功！")
                    window.location.href="userAdmin.html";
                }
            },
            error:function(data){
                alert("对不起，没有进入回调");
            }
        });
    }

}