jQuery.noConflict();
jQuery(function () {
    getCart();
    getUserMessage();
});

//从后台获取购物车和用户
function getCart() {
    jQuery.post("/kgc-easybuy01/getCart",function (data) {
        //获取页面表格
        var tb=jQuery(".car_tab1").find("tbody");
        console.log(tb);
        //内容
        var str="";
        var map=data.cart.map;
        console.log(map);
        //总计
        var total=0.00;
        for(var key in map){
            //商品项
            var shopItem=map[key];
            //商品
            var product=shopItem.product;
            total+=shopItem.subtotal;
        //拼接商品项tr
            //先清空tb
        str="<tr><td align='center'><div class='c_s_img'><img src='" +
            "images/"+product.fileName +
            "' width='73' height='73' /></div>" +
            product.name +
            "</td><td align='center'>" +
            "￥"+product.price+
            "</td><td align='center'><div class='c_num'>" +
            shopItem.count+
            "</div></td><td align='center' style='color:#ff4e00;'>" +
            "￥"+shopItem.subtotal+
            "</td><td align='center'><input type='button' name='id'value='结算'><input type='hidden'class='unPchecked' value='" +
            product.id+
            "'></td></tr>";
            //向表格添加数据
            tb.append(str);
        }
        //总金额内容
        str="<tr height='70'><td colspan='4' style='font-family:'Microsoft YaHei'; border-bottom:0;'><span class='fr'>" +
            "付款金额" +
            "<b style='font-size:22px; color:#ff4e00;'>" +
            "￥"+0.00+
            "</b></span></td><td style='font-family:'Microsoft YaHei'; border-bottom:0;'><span class='fr'>" +
            "商品总价" +
            "<b style='font-size:22px; color:#ff4e00;'>" +
            "￥"+total+
            "</b></span></td></tr>";
        tb.append(str);
        //底部总金额
        var font=jQuery(".two_bg").find("table").find("font")[0];
        font.innerHTML='￥'+0.00;
        jQuery(".sp")[0].innerHTML='￥'+15.00;
        //购物车图标部分
        var i=0;
        jQuery(".cars").html("");
        for(var key in map){
            i++;
            console.log(map[key]);
            console.log(map[key].product);
            str="<li><div class='img'><a href='#'><img src='" +
                "images/"+map[key].product.fileName+
                "' width='58' height='58' /></a></div><div class='name'><a href='#'>" +
                map[key].product.name+
                "</a></div><div class='price'><font color='#ff4e00'>" +
                "￥"+map[key].subtotal+
                "</font>"+
                " X"+map[key].count+
                "</div></li>";
            jQuery(".cars").append(str);
        }
        jQuery(".car_t").find("span")[0].innerHTML=i;
        str="<div class='price_sum'>共计&nbsp; <font color='#ff4e00'>￥</font><span>" +
            data.cart.total+
            "</span></div><div class='price_a'><a href='/BuyCar.html'>去购物车结算</a></div>";
        jQuery(".dfoot").html(str);
    })
}
//从后台获取用户信息
function getUserMessage() {
    jQuery.ajaxSettings.async = false;
    jQuery.post("/kgc-easybuy01/getUserMessage",function (data) {
        console.log(data);
        jQuery(".selAdd").live("change",function () {
            selchange(data);
        })
        var tr=jQuery(".peo_tab").find("tr");
        tr.find("td")[1].innerHTML=data.userName;
        tr.find("td")[3].innerHTML=data.email;
        tr.find("td")[9].innerHTML=data.mobile;
        for(var i=0;i<data.addresses.length;i++){
            if(data.addresses[i].isDefault==1){
                tr.find("td")[5].innerHTML=data.addresses[i].address;
                var str="<option value='" +
                    i+
                    "' selected='selected'>地址" +
                    (i+1)+
                    "</option>";
                jQuery(".selAdd").append(str);
            }else{
                var str="<option value='" +
                    i+
                    "'>地址" +
                    (i+1)+
                    "</option>";
                jQuery(".selAdd").append(str);
            }
        }
    })
}
function selchange(data) {
    var i= jQuery('select option:selected').val();
    var tr=jQuery(".peo_tab").find("tr");
    tr.find("td")[5].innerHTML=data.addresses[i].address;
}
//跳转到BuyCar_Three
function goThree() {
    var item=jQuery(".Pchecked");
    if(item.length==0){
        return false;
    }else{
        //数据
        //配送方式
        var peisong=jQuery('input[name="ch"]:checked').parent().parent().find("b")[0].innerHTML;
        console.log(peisong);
        //支付方式
        var zhifu=jQuery(".checked")[0].innerHTML;
        console.log(zhifu.substring(0,zhifu.indexOf('<')));
        //应付金额
        var money=jQuery(".sp")[0].innerHTML;
        console.log(money.substr(1,money.length-1));
        //支付商品项id
        var ids=new Array();

        for(var i=0;i<item.length;i++){
            ids[i]=item[i].value;
        }
        var serialNumber="";
            //向后台发送数据生成订单
            jQuery.post("/kgc-easybuy01/addOrder", {total:money.substr(1,money.length-1),ids:""+ids}, function (data) {
                console.log(data)
                serialNumber=data.order.serialNumber;
                window.location.href='/BuyCar_Three.html?peisong='+peisong+'&zhifu='+zhifu.substring(0,zhifu.indexOf('<'))+'&money='+money.substr(1,money.length-1)+
                    '&serialNumber='+serialNumber;
            });

        /*   window.location.href='/BuyCar_Three.html?peisong='+ encodeURI(encodeURI(peisong))+'&zhifu='+ encodeURI(encodeURI(zhifu.substring(0,zhifu.indexOf('<'))))+'&money='+money.substr(1,money.length-1)+
         '&ids='+ids;*/
    }
}