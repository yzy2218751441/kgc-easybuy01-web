//分类级别选择事件
jQuery(".type").change(function() {
    jQuery(".parentId").empty();
    var type=jQuery(".type").val();
    if(type=="1"){
        jQuery(".parentType").hide();
    }else{
        jQuery(".parentType").show();
        var dataJ={"type":type};
        jQuery.ajax({
            url:"/kgc-easybuy01/getCategory",
            dataType:"json",
            data:dataJ,
            success:function (categorys) {
                for(i=0;i<categorys.length;i++){
                    jQuery(".parentId").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
                }
            },
            error:function () {
                alert("回调失败！")
            }
        })
    }
});

//分类名称可用性判断
jQuery(".name").blur(function(){
    var name=jQuery(this).val();
    jQuery("#name").text("");
    if(name==""){
        jQuery("#name").text("用户名不能为空");
    }else{
        jQuery.ajax({
            url:"/kgc-easybuy01/getCategoryTest",
            data:"name="+name,
            success:function(result){
                if(result=="0"){
                    jQuery("#name").text("");
                }else{
                    jQuery("#name").text("分类名已存在");
                }
            },
            error:function(data){
                alert("回调失败！");
            }
        });
    }
})

//添加分类
function addCategory() {
    var name=jQuery(".name").val();
    var parentId=parseInt(jQuery(".parentId").val());
    var type=parseInt(jQuery(".type").val());
    if(type==1){
        parentId=0;
    }
    var dataJ={"name":name,"parentId":parentId,"type":type};
    if(name!=""&&jQuery("#name").val()==""){
        //添加操作
        jQuery.ajax({
            url:"/kgc-easybuy01/addCategory",
            dataType:"json",
            data:dataJ,
            success:function(result){
                if(parseInt(result)!=0){
                    alert("添加成功！");
                    window.location.href="categoryAdmin.html?pageNow=1";
                }
            },
            error:function(data){
                alert("回调失败！");
            }
        });
    }
}