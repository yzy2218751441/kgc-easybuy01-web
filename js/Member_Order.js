jQuery.noConflict();
jQuery(function () {
    getPersonOrder(1);
});
function getPersonOrder(pageNow) {
    jQuery.post("/kgc-easybuy01/getPersonOrder",{pageNow:pageNow},function (data) {
        console.log(data);
        if(data.list.length!=0){
            jQuery(".mem_tit").html(data.list[0].loginName+"的订单");
            var str="";
            jQuery(".order_tab").html("");
            for(var i=0;i<data.list.length;i++){
                str="<tr style='background-color: #fabeb8'><td colspan='2' width='50%'>订单号:" +
                    data.list[i].serialNumber+
                    "</td><td  width='25%'>地址:" +
                    data.list[i].userAddress+
                    "</td><td  width='25%'>￥" +
                    data.list[i].cost+
                    "</td></tr><tr><td>商品名称</td><td>商品图片</td><td>数量</td><td>价格</td></tr>";
                jQuery(".order_tab").append(str);
               for(var j=0;j<data.list[i].detail.length;j++){
                    str="<tr><td>" +
                        data.list[i].detail[j].product.name+
                        "</td><td><img src='images/" +
                        data.list[i].detail[j].product.fileName+
                        "' style='width: 50px;height: 50px'/></td><td>" +
                        data.list[i].detail[j].quantity+
                        "</td><td>" +
                        data.list[i].detail[j].cost+
                        "</td></tr>";
                   jQuery(".order_tab").append(str);
               }
            }
          bang(data);
        }else{
            jQuery(".mem_tit").html("");
            jQuery(".order_tab").html("");
            jQuery(".cate_listp").html("");
        }
    })
}
//绑定函数
function bang(data) {
    jQuery(".sp1").html('共'+data.rowCount+'条');
    jQuery(".sp2").html(data.pageNow+'/'+data.totolePage+"页");
    jQuery(".pageNo").val("");
    jQuery(".shou").live("click",function () {
        getPersonOrder(1);
    })
    if(data.pageNow!=1){
        jQuery(".shang").die().live("click",function () {
            getPersonOrder(parseInt(data.pageNow)-1);
        })
    }
    if(data.pageNow!=data.totolePage){
        jQuery(".xia").die().live("click",function () {
            getPersonOrder(parseInt(data.pageNow)+1);
        })
    }
    jQuery(".wei").die().live("click",function () {
        getPersonOrder(parseInt(data.totolePage));
    })

    jQuery(".pageNo").die().live("blur",function () {
        var pageNow=jQuery(this).val();
        var reg=new RegExp("\\d{1,"+data.totolePage+"}");
        if(pageNow.match(reg)){
            jQuery(".Gopage").die().live("click",function () {
                getPersonOrder(parseInt(pageNow));
            })
        }
    })
    jQuery(".shou").css("display","inline ");
    jQuery(".shang").css("display","inline ");
    jQuery(".xia").css("display","inline ");
    jQuery(".wei").css("display","inline ");
    if(data.pageNow==1){
        jQuery(".shou").css("display","none");
        jQuery(".shang").css("display","none");
    }
    if(data.pageNow==data.totolePage){
        jQuery(".xia").css("display","none");
        jQuery(".wei").css("display","none");
    }
}
