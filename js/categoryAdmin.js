//从URL中获取参数
(function (jQuery) {
    jQuery.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
})(jQuery);

//首次查询查出前五个分类
function getCategory() {
    //得到页码数
    var pageNow=jQuery.getUrlParam("pageNow");
    selectCategoryByPage(pageNow);
}

//分页查询函数
function selectCategoryByPage(pageNow) {
    var dataJ={"pageNow":pageNow};
    jQuery.ajax({
        url:"/kgc-easybuy01/selectCategoryByPage",
        dataType:"json",
        data:dataJ,
        success:function (pageBean) {

            //将查询结果插入页面中
            //清空列表
            jQuery(".mem_tab").empty();

            var $tr1=jQuery('<tr>'+
                '<td width="30%" >分类名称</td>'+
                '<td width="30%">父级分类</td>'+
                '<td width="20%" >分类级别</td>'+
                '<td width="20%">操作</td>'+
                '</tr>');
            jQuery(".mem_tab").append($tr1);

            var categorys=pageBean.list;
            for (i=0;i<categorys.length;i++){
                var $td1=jQuery('<td width="30%" >'+categorys[i].name+'</td>');
                var $td2;
                if(categorys[i].parentName==null){
                    $td2=jQuery('<td width="30%" >无</td>');
                }else{
                    $td2=jQuery('<td width="30%" >'+categorys[i].parentName+'</td>');
                }
                var $td3;
                if(categorys[i].type=="1"){
                    $td3=jQuery('<td width="20%" >一级分类</td>');
                }else if(categorys[i].type=="2"){
                    $td3=jQuery('<td width="20%" >二级分类</td>');
                }else if(categorys[i].type=="3"){
                    $td3=jQuery('<td width="20%" >三级分类</td>');
                }
                var $td4=jQuery('<td width="20%"><a href="javascript:;" onclick="deleteCategory(this,'+categorys[i].id+','+categorys[i].type+')">删除</a></td>');
                var $tr=jQuery('<tr></tr>');
                $tr.append($td1);
                $tr.append($td2);
                $tr.append($td3);
                $tr.append($td4);
                jQuery(".mem_tab").append($tr);
            }
            //删除分页按钮
            jQuery(".cate_listp").remove();
            //拼分页按钮
            var pageNow=parseInt(pageBean.pageNow);
            var totolePage=parseInt(pageBean.totolePage);
            var $p;
            if (pageNow==1){
                $p=jQuery('<p class="cate_listp">共'+pageBean.rowCount+'条&nbsp;&nbsp;&nbsp;'+pageBean.pageNow+'/'+pageBean.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);"onclick="method1(3)">下一页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(4)">尾页</a> '+
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>');
            }else if(1<pageNow &&pageNow<totolePage){
                $p=jQuery('<p class="cate_listp">共'+pageBean.rowCount+'条&nbsp;&nbsp;&nbsp;'+pageBean.pageNow+'/'+pageBean.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);" onclick="method1(1)">首页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(2)">上一页</a>&nbsp;'+
                    '<a href="javascript:void(0);"onclick="method1(3)">下一页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(4)">尾页</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '+
                    '跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>');

            }else if(pageNow==totolePage){
                $p=jQuery('<p class="cate_listp">共'+pageBean.rowCount+'条&nbsp;&nbsp;&nbsp;'+pageBean.pageNow+'/'+pageBean.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);" onclick="method1(1)">首页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(2)">上一页</a>&nbsp;'+
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>')
            }
            jQuery(".mem_tab").after($p);
            //将总页数放入页面隐藏域中
            jQuery("#totolePage").val(pageBean.totolePage);

        },
        error:function () {
            alert("回调失败！");
        }
    })
}


//分页跳转函数
function method1(type) {
    //从URL中获取参数
    (function (jQuery) {
        jQuery.getUrlParam = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
    })(jQuery);

    //获取所需的参数
    var pageNow=parseInt(jQuery.getUrlParam("pageNow"));
    if (type==1){
        pageNow=1;
        jQuery("#pageNow").val(pageNow);
    }else if(type==2){
        pageNow=pageNow-1;
        jQuery("#pageNow").val(pageNow);
    }else if (type==3){
        pageNow=pageNow+1;
        jQuery("#pageNow").val(pageNow);
    }else if (type==4){
        pageNow=parseInt(jQuery("#totolePage").val());
        jQuery("#pageNow").val(pageNow);
    }else if(type==5){
        var pageNo=parseInt(jQuery("#pageNo").val());
        var re =  /^[1-9]\d*$/;
        if(!re.test(pageNo)||pageNo>totolePage){
            //如果输入的不是正整数或者大于总页数，默认跳到第一页
            pageNow=1;
        }else{
            pageNow=pageNo;
        }
    }
    window.location.href="categoryAdmin.html?pageNow="+pageNow;
}

//删除商品
function deleteCategory($this,id,type) {
    var r = confirm("确认删除该分类吗？");
    if (r == true) {
        var dataJ={"id":id,"type":type};
        jQuery.ajax({
            url:"/kgc-easybuy01/deleteCategory",
            dataType:"json",
            data:dataJ,
            success:function (map) {
                if(parseInt(map.result)==1){
                    //删除成功将元素移除
                    alert("删除成功！")
                    jQuery($this).parent().parent().remove();
                }else if(parseInt(map.result)==0){
                    alert("该分类目前不能被删除！")
                }
            },
            error:function () {
                alert("回调失败！");
            }
        })
    }
}