//表单回显
function getNews() {
    //从URL中获取参数
    (function (jQuery) {
        jQuery.getUrlParam = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
    })(jQuery);

    //拿到回显id
    var id=jQuery.getUrlParam("id");
    var dataJ={"id":id};
    jQuery.ajax({
        url:"/kgc-easybuy01/getNews",
        dataType:"json",
        data:dataJ,
        success: function (news) {
            jQuery("#title").val(news.title);
            jQuery("#content").val(news.content);
        },
        error:function () {
            alert("回调失败！")
        }
    })
}

//修改
function newsUpdate() {
    var title=jQuery("#title").val();
    var content=jQuery("#content").val();
    var id=jQuery.getUrlParam("id");
    if (title==''){
        alert("请输入新闻标题!");
    }else if (content==''){
        alert("请输入新闻内容！")
    }else{
        var dataJ={"title":title,"content":content,"id":id};
        jQuery.ajax({
            url:"/kgc-easybuy01/updateNews",
            dataType:"json",
            data:dataJ,
            success:function(result){
                if(parseInt(result)!=0){
                    alert("修改成功！");
                    //返回上一页并刷新
                    window.location.href=document.referrer;
                }
            },
            error:function(data){
                alert("回调失败！");
            }
        });
    }
}