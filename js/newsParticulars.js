//表单回显
function getNews() {
    //从URL中获取参数
    (function (jQuery) {
        jQuery.getUrlParam = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
    })(jQuery);

    //拿到回显id
    var id=jQuery.getUrlParam("id");
    var dataJ={"id":id};
    jQuery.ajax({
        url:"/kgc-easybuy01/getNews",
        dataType:"json",
        data:dataJ,
        success: function (news) {
            jQuery("#title").val(news.title);
            jQuery("#content").val(news.content);
        },
        error:function () {
            alert("回调失败！")
        }
    })
}

//返回
function newsReturn() {
    window.history.back(-1);
}