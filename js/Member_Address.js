jQuery.noConflict();
jQuery(function () {
    getAddress()
})
function getAddress() {
    jQuery.post("/kgc-easybuy01/getAddress",function (data) {
        console.log(data);
        show(data);

    })
}
//展示页面
function show(data) {
    jQuery(".daddress").html("");
    var str="";
    for(var i=0;i<data.addresses.length;i++){
        str="<dt class='dl" +
            i+
            "'>地址" +
            (i+1)+
            "</dt><dd style='display: none'class='" +
            i+
            "'><input type='hidden' value='" +
            data.addresses[i].id+
            "'><table border='0' class='add_t' align='center' style='width:98%; margin:10px auto;' cellspacing='0' cellpadding='0'><tr><td colspan='2' style='font-size:14px; color:#ff4e00;'>" +
            data.addresses[i].remark+
            "</td></tr><tr><td align='right' width='80'>收货人姓名：</td><td>" +
            data.userName+
            "</td></tr><tr><td align='right'>地址：</td><td>" +
            data.addresses[i].address+
            "</td></tr><tr><td align='right'>电话：</td><td>" +
            data.mobile+
            "</td></tr><tr><td align='right'>电子邮箱：</td><td>" +
            data.email+
            "</td></tr></table><p align='right'><a href='#' class='dft'style='color:#ff4e00;'>设为默认</a>&nbsp; &nbsp; &nbsp; &nbsp; <a class='update' style='color:#ff4e00;'>编辑</a>&nbsp; &nbsp; &nbsp; &nbsp;<a class='del' style='color:#ff4e00;'>删除</a>&nbsp; &nbsp; &nbsp; &nbsp;</p></dd>";
        jQuery(".daddress").append(str);
        if(data.addresses[i].isDefault==1){
            jQuery("."+i).find("a")[0].style.display="none";
            jQuery("."+i).find("a")[2].style.display="none";
            //将默认地址id保存在作用域
            jQuery(".m_right").find("input")[0].setAttribute("value",data.addresses[i].id);
        }
    }
    //新增部分
    str="<table border='0' class='add_tab2' style='width:339px;display:block' cellspacing='0' cellpadding='0'><tr>" +
        "" +
        "<td align='right'>配送地址:</td><td style='font-family:'宋体';'><input type='text' value='' class='addipt2' /><span></span></td></tr><tr>" +
        "" +
        "<td align='right'>地址备注:</td><td style='font-family:'宋体';'><input type='text' value='' /></td></tr></table><p align='center'>" +
        "" +
        "<a class='addb2'>增加</a></p>";
        jQuery(".addFoot").html(str);
    showdl(data);
}
//绑定点击事件
function showdl(data) {
    jQuery(".daddress").find("dt").live("click",function () {
        jQuery(this).toggle(function () {
            jQuery(this).next().css("display","inline");
        },function () {
            jQuery(this).next().css("display","none");
        })
        jQuery(this).trigger('click');
    });
    jQuery(".addFootC").die().live("click",function () {
        jQuery(this).toggle(function () {
            jQuery(".addFoot").css("display","inline");
        },function () {
            jQuery(".addFoot").css("display","none");
        })
        jQuery(this).trigger('click');
    });
    jQuery(".addFoot").css("display","none");
    jQuery(".addb2").die().live("click",function () {
        addAddress(jQuery(this));
    });
    jQuery(".addipt2").die().live("blur",function () {
        if(jQuery(this).val()==""){
            console.log(jQuery(this).next());
            jQuery(this).next().html("地址不能为空");
            jQuery(this).next().css("color","red");
        }else{
            jQuery(this).next().html("");
        }
    });
    jQuery(".update").die().live("click",function () {
        showupd(data,jQuery(this));
    });
    jQuery(".dft").die().live("click",function () {
        upddft(jQuery(this));
        var cl='dl'+jQuery(this).parent().parent().attr("class");
        jQuery("."+cl).click();
    })
    jQuery(".del").die().live("click",function () {
        delA(jQuery(this));
    })
}
//删除
function delA(jqthis) {
    //获取商品id
    var id=jqthis.parent().parent().find("input")[0].value;
    if(confirm("确定删除这条地址？")){
        jQuery.post("/kgc-easybuy01/delA",{id:id},function (data) {
            console.log(data);
            getAddress();
        })
    }
}
//修改默认
function upddft(jqthis) {
    //获取商品id
    var id=jqthis.parent().parent().find("input")[0].value;
     var dftid= jQuery(".m_right").find("input")[0].getAttribute("value");
    jQuery.ajaxSettings.async = false;
    jQuery.post("/kgc-easybuy01/upddft",{id:id,dftid:dftid},function (data) {
        console.log(data);
        getAddress();
    })
}
//新增函数
function addAddress(addb2) {
    var addresses=addb2.parent().prev().find("input")[0].value;
    var remark=addb2.parent().prev().find("input")[1].value;
    if(addresses!=""){
        jQuery.post("/kgc-easybuy01/addAddress",{address:addresses,remark:remark},function (data) {
            console.log(data);
            getAddress();
        })
    }
}
//展示修改部分
function showupd(data,jqthis) {

    var str="<table border='0' class='add_tab' style='width:339px;display:block' cellspacing='0' cellpadding='0'><tr>" +
        "" +
        "<td align='right'>配送地址:</td><td style='font-family:'宋体';'><input type='text' value='' class='addipt' /><span></span></td></tr><tr>" +
        "" +
        "<td align='right'>地址备注:</td><td style='font-family:'宋体';'><input type='text' value='' /></td></tr></table><p>" +
        "" +
        "<a class='back'>返回</a>&nbsp; &nbsp; <a class='addb'>确认修改</a></p>";
    //获取商品id
    var id=jqthis.parent().parent().find("input")[0].value;
    jqthis.parent().parent().html(str);

    jQuery(".back").die().live("click",function () {
        //展示原来页面并点击
        show(data);
        var cl='dl'+jQuery(this).parent().parent().attr("class");
        jQuery("."+cl).click();
    });

    jQuery(".addipt").die().live("blur",function () {
        if(jQuery(this).val()==""){
            console.log(jQuery(this).next());
            jQuery(this).next().html("地址不能为空");
            jQuery(this).next().css("color","red");
        }else{
            jQuery(this).next().html("");
        }
    });

    jQuery(".addb").die().live("click",function () {
       upd(jQuery(this),id);
        var cl='dl'+jQuery(this).parent().parent().attr("class");
        jQuery("."+cl).click();
    })
}
//提交修改数据
function upd(dd,id) {
    var addresses=dd.parent().prev().find("input")[0].value;
    var remark=dd.parent().prev().find("input")[1].value;
    if(addresses!=""){
        jQuery.ajaxSettings.async = false;
        jQuery.post("/kgc-easybuy01/upd",{id:id,address:addresses,remark:remark},function (map) {
            console.log(map);
                getAddress();
        })
     /*   jQuery.ajaxSettings.async = true;*/
    }else{
        alert("地址为空");
    }
}
