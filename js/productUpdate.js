//拿到一级分类并放入一级下拉列表中
function getCategoryLevel1Id() {
    jQuery.ajax({
        url:"/kgc-easybuy01/getCategoryLevel1Id",
        success:function (categorys) {
            for(i=0;i<categorys.length;i++){
                jQuery(".categoryLevel1Id").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
            }
            getProduct();
        },
        error:function () {
            alert("回调失败！")
        }
    })
}

//表单回显
function getProduct() {
    //从URL中获取参数
    (function (jQuery) {
        jQuery.getUrlParam = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
    })(jQuery);

    //拿到回显id
    var id=jQuery.getUrlParam("id");
    var dataJ={"id":id};
    jQuery.ajax({
        url:"/kgc-easybuy01/getProduct",
        dataType:"json",
        data:dataJ,
        success:function (result) {
            var product=result.product;
            //默认选中商品一级分类
            jQuery(".categoryLevel1Id").val(product.categoryLevel1Id);
            //拿到二级分类
            var parentId=jQuery(".categoryLevel1Id").val();
            var dataJ2={"parentId":parentId};
            jQuery.ajax({
                url:"/kgc-easybuy01/getCategoryLevel2Id",
                dataType:"json",
                data:dataJ2,
                success:function (categorys) {
                    //回显三级列表
                    for(i=0;i<categorys.length;i++){
                        jQuery(".categoryLevel2Id").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
                    }
                    //默认选中商品二级分类
                    jQuery(".categoryLevel2Id").val(product.categoryLevel2Id);
                    //查出三级分类
                    var parentId3=jQuery(".categoryLevel2Id").val();
                    var dataJ3={"parentId":parentId3};
                    jQuery.ajax({
                        url:"/kgc-easybuy01/getCategoryLevel2Id",
                        dataType:"json",
                        data:dataJ3,
                        success:function (categorys) {
                            for(i=0;i<categorys.length;i++){
                                jQuery(".categoryLevel3Id").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
                            }
                            //默认选中三级分类
                            jQuery(".categoryLevel3Id").val(product.categoryLevel3Id);
                        },
                        error:function () {
                            alert("回调失败！")
                        }
                    })

                },
                error:function () {
                    alert("回调失败！")
                }
            })
            //回显
            jQuery(".name").val(product.name);
            jQuery(".id").val(product.id);
            jQuery(".price").val(product.price);
            jQuery(".stock").val(product.stock);
            jQuery(".description").val(product.description);
            jQuery("#img").attr('src',"/images/"+product.fileName);

        },
        error:function () {
            alert("回调失败！")
        }
    })
}







//一级分类选择后拿到二级分类
jQuery(".categoryLevel1Id").click(function() {
    jQuery(".categoryLevel2Id").empty();
    jQuery(".categoryLevel3Id").empty();
    var parentId=jQuery(".categoryLevel1Id").val();
    var dataJ={"parentId":parentId};
    jQuery.ajax({
        url:"/kgc-easybuy01/getCategoryLevel2Id",
        dataType:"json",
        data:dataJ,
        success:function (categorys) {
            for(i=0;i<categorys.length;i++){
                jQuery(".categoryLevel2Id").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
            }
        },
        error:function () {
            alert("回调失败！")
        }
    })
});

//二级分类选择后拿到三级分类
jQuery(".categoryLevel2Id").click(function() {
    jQuery(".categoryLevel3Id").empty();
    var parentId=jQuery(".categoryLevel2Id").val();
    var dataJ={"parentId":parentId};
    jQuery.ajax({
        url:"/kgc-easybuy01/getCategoryLevel2Id",
        dataType:"json",
        data:dataJ,
        success:function (categorys) {
            for(i=0;i<categorys.length;i++){
                jQuery(".categoryLevel3Id").append("<option value='"+categorys[i].id+"'>"+categorys[i].name+"</option>");
            }
        },
        error:function () {
            alert("没有子分类啦！")
        }
    })
});

//商品名称非空
jQuery(".name").blur(function(){
    var name=jQuery(this).val();
    jQuery("#name").text("");
    if(name==""){
        jQuery("#name").text("商品名不能为空");
    }else{
        jQuery("#name").text("");
    }
})

//单价非空判断
jQuery(".price").blur(function(){
    var price=jQuery(this).val();
    jQuery("#price").text("");
    if(price==""){
        jQuery("#price").text("商品单价不能为空");
    }else{
        var re =  /^[+]{0,1}(\d+)$|^[+]{0,1}(\d+\.\d+)$/;
        if(!re.test(price)){
            //如果输入的不是正浮点数
            jQuery("#price").text("请输入正数！");
        }else{
            jQuery("#price").text("");
        }
    }
})

//库存非空判断
jQuery(".stock").blur(function(){
    var stock=jQuery(this).val();
    jQuery("#stock").text("");
    if(stock==""){
        jQuery("#stock").text("商品名不能为空");
    }else{
        var re =  /^[1-9]\d*$/;
        if(!re.test(stock)){
            //如果输入的不是正整数
            jQuery("#stock").text("请输入正整数！");
        }else{
            jQuery("#stock").text("");
        }
    }
})

//图片上传前端验证
function upLoad(){
    jQuery("#img").attr('src',"");
    jQuery("#fileName1").text("");
    //使用类型判断浏览器是否支持base64转换
    if (typeof (FileReader) === 'undefined') {
        alert("抱歉，你的浏览器不支持文件读取，请更新浏览器操作！");
    }else{
        var fileInput = document.getElementById("fileName");
        var file = fileInput.files[0];
        //000.获取文件
        var fileSize = fileInput.files[0].size;	//文件大小
        var size = fileSize / 1024*1024; 	//和上面那句一样1.5M约等于160 0000
        //001.文件格式校验
        if (!/image\/\w+/.test(file.type)) {
            jQuery("#fileName1").text("请确保文件为图像类型！");
        }else if(size>1000000){
            jQuery("#fileName1").text("上传图片大小不得大于1M！");
        }else{
            //创建读取文件的对象
            var reader = new FileReader();
            //创建文件读取相关的变量
            var imgFile;
            //为文件读取成功设置事件
            reader.onload=function(e) {
                // alert('文件读取完成');
                imgFile = e.target.result;
                console.log(imgFile);
                jQuery("#img").attr('src',imgFile);
            };
            jQuery("#img").show();
            //正式读取文件
            reader.readAsDataURL(file);
        }
    }
}

//修改商品
function updateProduct() {
    if(jQuery(".name").val()!=""&&jQuery("#name").text()==""&&jQuery(".price").val()!=""&&jQuery("#price").text()==""&&
        jQuery(".stock").val()!=""&&jQuery("#stock").text()==""&&jQuery("#fileName1").text()==""){

        var formData=new FormData(jQuery(".mem_tab")[0]);
        jQuery.ajax({
            url: "/kgc-easybuy01/updateProduct",
            type: "POST",
            enctype: 'multipart/form-data',
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (parseInt(result) != 0) {
                    alert("修改成功！");
                    window.location.href = "productAdmin.html?pageNow=1";
                }
            },
            error: function (data) {
                alert("回调失败！");
            }
        });
    }
}