jQuery.noConflict();
//拿到所有用户信息

//分页查询查出五条用户信息
function getUserByPage() {
    //得到页码数
    var pageNow=jQuery("#pageNow").val();
    var dataJ={"pageNow":pageNow};
    selectUserByPage(dataJ)
}

//分页查询
function selectUserByPage(dataJ) {
    jQuery.ajax({
        url:"/kgc-easybuy01/selectUserByPage",
        dataType:"json",
        data:dataJ,
        success:function (pageBean) {
            //将查询结果插入页面中
            //清空列表
            jQuery(".mem_tab").empty();

            var $tr1=jQuery('<tr>'+
                '<td width="145" class="td_bg">会员名</td>'+
                '<td width="145">真实姓名</td>'+
                '<td width="145" class="td_bg">性别</td>'+
                '<td width="145">类型</td>'+
                '<td width="145" class="td_bg">修改</td>'+
                '<td width="145">删除</td>'+
                '</tr>');
            jQuery(".mem_tab").append($tr1);

            var users=pageBean.list;
            for (i=0;i<users.length;i++){
                var $td1=jQuery('<td width="145" class="td_bg">'+users[i].loginName+'</td>');
                var $td2=jQuery('<td width="145">'+users[i].userName+'</td>');
                var $td3;
                if (users[i].sex=="1"){
                    $td3=jQuery('<td width="145" class="td_bg">男</td>');
                }else if (users[i].sex=="0"){
                    $td3=jQuery('<td width="145" class="td_bg">女</td>');
                }
                var $td4;
                var $td6;
                if(users[i].type=="1"){
                    $td4=jQuery('<td width="145">管理员</td>');
                    $td6=jQuery('<td width="145"><a href="#"> </a></td>');
                }else if (users[i].type=="0"){
                    $td4=jQuery('<td width="145">普通会员</td>');
                    $td6=jQuery('<td width="145"><a href="javascript:;" onclick="deleteUser(this,'+users[i].id+')">删除</a></td>');
                }
                var $td5=jQuery('<td width="145" class="td_bg"><a href="/userUpdate.html?id='+users[i].id+'">修改</a></td>');
                var $tr=jQuery('<tr></tr>');
                $tr.append($td1);
                $tr.append($td2);
                $tr.append($td3);
                $tr.append($td4);
                $tr.append($td5);
                $tr.append($td6);
                jQuery(".mem_tab").append($tr);
            }
            //删除分页按钮
            jQuery(".cate_listp").remove();
            //拼分页按钮
            var pageNow=parseInt(pageBean.pageNow);
            var totolePage=parseInt(pageBean.totolePage);
            var $p;
            if (pageNow==1){
                $p=jQuery('<p class="cate_listp">共'+pageBean.rowCount+'条&nbsp;&nbsp;&nbsp;'+pageBean.pageNow+'/'+pageBean.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);"onclick="method1(3)">下一页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(4)">尾页</a> '+
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>');
            }else if(1<pageNow &&pageNow<totolePage){
                $p=jQuery('<p class="cate_listp">共'+pageBean.rowCount+'条&nbsp;&nbsp;&nbsp;'+pageBean.pageNow+'/'+pageBean.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);" onclick="method1(1)">首页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(2)">上一页</a>&nbsp;'+
                    '<a href="javascript:void(0);"onclick="method1(3)">下一页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(4)">尾页</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '+
                    '跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>');

            }else if(pageNow==totolePage){
                $p=jQuery('<p class="cate_listp">共'+pageBean.rowCount+'条&nbsp;&nbsp;&nbsp;'+pageBean.pageNow+'/'+pageBean.totolePage+'页&nbsp;&nbsp;&nbsp;'+
                    '<a href="javascript:void(0);" onclick="method1(1)">首页</a>&nbsp;<a href="javascript:void(0);"onclick="method1(2)">上一页</a>&nbsp;'+
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳转<input type="text" size="4" id="pageNo" name="goPage"><input type="button" value="GO" id="Gopage"onclick="method1(5)"></p>')
            }
            jQuery(".mem_tab").after($p);
            //将总页数放入页面隐藏域中
            jQuery("#totolePage").val(pageBean.totolePage);
        },
        error:function () {
            alert("回调失败！")
        }
    })
}

//分页跳转函数
function method1(type) {
    //获取所需的参数
    var pageNow=parseInt(jQuery("#pageNow").val());
    if (type==1){
        pageNow=1;
        jQuery("#pageNow").val(pageNow);
    }else if(type==2){
        pageNow=pageNow-1;
        jQuery("#pageNow").val(pageNow);
    }else if (type==3){
        pageNow=pageNow+1;
        jQuery("#pageNow").val(pageNow);
    }else if (type==4){
        pageNow=parseInt(jQuery("#totolePage").val());
        jQuery("#pageNow").val(pageNow);
    }else if(type==5){
        var pageNo=parseInt(jQuery("#pageNo").val());
        var re =  /^[1-9]\d*$/;
        if(!re.test(pageNo)||pageNo>totolePage){
            //如果输入的不是正整数或者大于总页数，默认跳到第一页
            pageNow=1;
        }else{
            pageNow=pageNo;
        }
        jQuery("#pageNow").val(pageNow);
    }
    var dataJ2={"pageNow":pageNow};
    selectUserByPage(dataJ2);

}

//删除操作
function deleteUser($this,id) {
    alert("11");
    var dataJ={"id":id};
    jQuery.ajax({
        url:"/kgc-easybuy01/deleteUser",
        dataType:"json",
        data:dataJ,
        success:function (map) {
            if(parseInt(map.result)>0){
                //删除成功将元素移除
                alert("删除成功！")
                jQuery($this).parent().parent().remove();
            }
        },
        error:function () {
            alert("回调失败！");
        }
    })
}
