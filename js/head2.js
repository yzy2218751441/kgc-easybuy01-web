jQuery.noConflict();
jQuery(function () {
    gUser();
    getCart();

});
//拿到用户登陆状态
function gUser(){
    jQuery.ajax({
        url:"/kgc-easybuy01/getUser",
        success:function (user) {
            if(user.loginName==null){
                //未登陆
                jQuery(".car_bg").html("<div class='un_login'>还未登录！<a href='/Login.html' style='color:#ff4e00;'>马上登录</a> 查看购物车！</div><ul class='cars'></ul><div class='dfoot'></div>");
                //0表示未登录
                jQuery(".car_t").find("input")[0].value="0";
            }else if (user.type==1){
                //管理员
                jQuery(".car_bg").html("<div class='un_login'>购物车！</div><ul class='cars'></ul><div class='dfoot'></div>");
                jQuery(".car_t").find("input")[0].value="1";
            }else{
                //普通用户
                jQuery(".car_bg").html("<div class='un_login'>购物车！</div><ul class='cars'></ul><div class='dfoot'></div>");
                jQuery(".car_t").find("input")[0].value="2";
            }
        },
        error:function () {
            alert("回调失败！")
        }
    });
}
//从后台拿到购物车和用户信息
function getCart() {
    jQuery.post("/kgc-easybuy01/getCart",function (data) {
        var str='';
        var map =data.cart.map;
        var i=0;
        jQuery(".cars").html("");
        for(var key in map){
            i++;
            console.log(map[key]);
            console.log(map[key].product);
            str="<li><div class='img'><a href='#'><img src='" +
                "images/"+map[key].product.fileName+
                "' width='58' height='58' /></a></div><div class='name'><a href='#'>" +
                map[key].product.name+
                "</a></div><div class='price'><font color='#ff4e00'>" +
                "￥"+map[key].subtotal+
                "</font>"+
                " X"+map[key].count+
                "</div></li>";
            jQuery(".cars").append(str);
        }
        jQuery(".car_t").find("span")[0].innerHTML=i;
        str="<div class='price_sum'>共计&nbsp; <font color='#ff4e00'>￥</font><span>" +
            data.cart.total+
            "</span></div><div class='price_a'><a href='/BuyCar.html'>去购物车结算</a></div>";
        jQuery(".dfoot").html(str);
    })
}
//测试
function search() {
    (function (jQuery) {
        jQuery.getUrlParam = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
    })(jQuery);
    var categoryLevel=jQuery.getUrlParam("categoryLevel");
    var categoryId=jQuery.getUrlParam("categoryId");
    var selectString=jQuery("#selectInput").val();
    window.location.href="searchProduct.html?categoryLevel="+categoryLevel+"&categoryId="+categoryId+"&selectString="+encodeURI(encodeURI(selectString));
}