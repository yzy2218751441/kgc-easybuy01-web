//对登录名做验证
jQuery(".loginName").blur(function(){
    var loginName=jQuery(this).val();
    var loginName1=jQuery("#loginName1").val();
    jQuery("#loginName").text("");
    if(loginName==""){
        jQuery("#loginName").text("用户名不能为空");
    }else{
        jQuery.ajax({
            url:"/kgc-easybuy01/registTest",
            data:"loginName="+loginName,
            success:function(user){
                if(user.loginName==null||user.loginName==loginName1){
                    jQuery("#loginName").text("");
                }else{
                    jQuery("#loginName").text("用户名不可用");
                }
            },
            error:function(data){
                alert("回调失败！");
            }
        });
    }
})

//身份证验证
jQuery(".identityCode").blur(function(){
    var identityCode=jQuery(this).val();
    var patrn=/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    var result=patrn.test(identityCode);
    jQuery("#identityCode").text("");
    if(identityCode==""){
        jQuery("#identityCode").text("身份证不能为空");
    }else if(result==false){
        jQuery("#identityCode").text("身份证格式不正确");
    }
})
//邮箱验证
jQuery(".email").blur(function(){
    var email=jQuery(this).val();
    var patrn=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
    var result=patrn.test(email);
    jQuery("#email").text("");
    if(email==""){
        jQuery("#email").text("邮箱不能为空");
    }else if(result==false){
        jQuery("#email").text("邮箱格式不正确");
    }
})
//手机号验证
jQuery(".mobile").blur(function(){
    var mobile=jQuery(this).val();
    var patrn=/^1(3|4|5|6|7|8|9)\d{9}$/;
    var result=patrn.test(mobile);
    jQuery("#mobile").text("");
    if(mobile==""){
        jQuery("#mobile").text("电话号码不能为空");
    }else if(result==false){
        jQuery("#mobile").text("电话号码格式错误");
    }
})
//密码验证
jQuery(".password1").blur(function(){
    var password1=jQuery(this).val();
    jQuery("#password1").text("");
    if(password1==""){
        jQuery("#password1").text("密码不能为空");
    }
})
//确认密码验证
jQuery(".password2").blur(function(){
    var password2=jQuery(this).val();
    var password1=jQuery(".password1").val();
    jQuery("#password2").text("");
    if(password2==""){
        jQuery("#password2").text("密码不能为空");
    }else if(password1!=password2){
        jQuery("#password2").text("两次输入密码不一致");
        jQuery("#password1").text("两次输入密码不一致");
    }else if(password1==password2){
        jQuery("#password2").text("");
        jQuery("#password1").text("");
    }
})

//添加用户
function addUser() {
    var loginName=jQuery(".loginName").val();
    var userName=jQuery(".userName").val();
    var email=jQuery(".email").val();
    var mobile=jQuery(".mobile").val();
    var identityCode=jQuery(".identityCode").val();
    var type=jQuery(".selector").val();
    var password=jQuery(".password1").val();
    if(loginName!=""&&email!=""&&mobile!=""&&identityCode!=""&&password!=""&&jQuery("#loginName").text()==""&&jQuery("#email").text()==""
        &&jQuery("#mobile").text()==""&&jQuery("#identityCode").text()==""&&jQuery("#password1").text()==""){
        var v_json={"password":password,"loginName":loginName,"userName":userName,"email":email,"mobile":mobile,"type":type,"identityCode":identityCode};
        jQuery.ajax({
            url:"/kgc-easybuy01/addUser",
            data: v_json,
            dataType : "json",
            success:function(map){
                if(parseInt(map.result)>0){
                    //修改成功，跳转到查询页
                    alert("添加成功！")
                    window.location.href="userAdmin.html";
                }
            },
            error:function(data){
                alert("对不起，没有进入回调");
            }
        });
    }
}